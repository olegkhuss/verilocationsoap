
### Sample usage

#### Include in composer.json
```bash
{
    .....
	"require": {
		"olegkhuss/verilocationsoap": "~0.1.0@dev"
	}
}
```
#### PHP code
```php
<?php

use Gelo\Verilocation\SoapClient as VerilocationClient;

$client = new VerilocationClient('http://www.verilocation.com/gps/xml/version_001.asmx?wsdl', array(
	// options
    // 'cache_wsdl' => WSDL_CACHE_MEMORY,
));
try {
    $client->doLogon('<username>', '<password>');

    // is webservice active?
    print_r($client->isWebserviceActive());
    // get all vehicles
    print_r($client->getVehicles());
    // get all vehicles extended
    print_r($client->getVehiclesExt());
    // get all vehicles extended
    print_r($client->getLocationsLatest());

} catch (\Exception $e) {
    // could not login, chekc username/password
    throw $e;
}
```