<?php

namespace Gelo\Verilocation;


/**
 * Verilocation service SoapClient
 * service wsdl http://www.verilocation.com/gps/xml/version_001.asmx?WSDL
 * 
 * @author Oleg Khussainov <olegkhuss@gmail.com>
 */
class SoapClient extends \SoapClient
{
    /**
     * Logon token
     * @var string
     */
    protected $logonToken;


    /**
     * @param  string $username Verilocation username
     * @param  string $password Verilocation password
     * @return void
     */
    public function doLogon($username, $password)
    {
        $object = $this->Logon(array(
            'Username' => $username, 
            'Password' => $password,
        ));
        if (empty($object->LogonResult)) {
            throw new \RuntimeException('The "LogonToken" response is empty (check your username or password).');
        }

        $this->logonToken = (string) $object->LogonResult;
    }

    /**
     * @return string
     */
    public function getLogonToken()
    {
        return $this->logonToken;
    }

    /**
     * @return \stdClass
     */
    public function isWebServiceActive()
    {
        return $this->WebServiceActive();
    }

    /**
     * @return \stdClass
     */
    public function getVehicles()
    {
        return $this->Vehicles(array('LogonToken' => $this->getLogonToken()));
    }

    /**
     * @return \stdClass
     */
    public function getVehiclesExt()
    {
        return $this->VehiclesExt(array('LogonToken' => $this->getLogonToken()));
    }

    /**
     * @return \stdClass
     */
    public function getLocationsLatest()
    {
        return $this->LocationsLatest(array('LogonToken' => $this->getLogonToken()));
    }
}